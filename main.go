package main

import (
	"fmt"
	"strconv"
)

func main() {
	fmt.Println("Hello world")
}
// simple logic
func FizzBuzzOld(num int) string { // 
	if num%3 == 0 && num%5 == 0 {
		return "FizzBuzz"
	} else if num%3 == 0 {
		return "Fizz"
	} else if num%5 == 0 {
		return "Buzz"
	} else {
		return fmt.Sprint(num)
	}
}

 // using only 1 if condition
func FizzBuzz1(num int) string {
	output := ""
	output += map[bool]string{num%3 == 0: "Fizz"}[true]
	output += map[bool]string{num%5 == 0: "Buzz"}[true]
	if output == "" {
		output = fmt.Sprint(num)
	}
	return output
}


func FizzBuzz(num int) string {
	output := ""
	output += map[bool]string{num%3 == 0: "Fizz"}[true]
	output += map[bool]string{num%5 == 0: "Buzz"}[true]

	outputMap := map[bool]string{
		true:  output,
		false: strconv.Itoa(num),
	}

	return fmt.Sprint(outputMap[output != ""])
}