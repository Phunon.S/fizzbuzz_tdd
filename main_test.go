package main

import (
	"testing"
)

func TestFizzBuzzCase1(t *testing.T) {
	result := FizzBuzz(1)
	expected := "1"

	if result != expected {
		t.Error("Result :", result, " Expected:", expected)
	}

	t.Log("Result :", result, " Expected:", expected)
}

func TestFizzBuzzCase2(t *testing.T) {
	result := FizzBuzz(2)
	expected := "2"

	if result != expected {
		t.Error("Result :", result, " Expected:", expected)
	}

	t.Log("Result :", result, " Expected:", expected)
}
func TestFizzBuzzCase3(t *testing.T) {
	result := FizzBuzz(3)
	expected := "Fizz"

	if result != expected {
		t.Error("Result :", result, " Expected:", expected)
	}
	t.Log("Result :", result, " Expected:", expected)
}

func TestFizzBuzzCase4(t *testing.T) {
	result := FizzBuzz(4)
	expected := "4"

	if result != expected {
		t.Error("Result :", result, " Expected:", expected)
	}
	t.Log("Result :", result, " Expected:", expected)
}

func TestFizzBuzzCase5(t *testing.T) {
	result := FizzBuzz(5)
	expected := "Buzz"

	if result != expected {
		t.Error("Result :", result, " Expected:", expected)
	}
	t.Log("Result :", result, " Expected:", expected)
}
func TestFizzBuzzCase6(t *testing.T) {
	result := FizzBuzz(6)
	expected := "Fizz"

	if result != expected {
		t.Error("Result :", result, " Expected:", expected)
	}
	t.Log("Result :", result, " Expected:", expected)
}

func TestFizzBuzzCase7(t *testing.T) {
	result := FizzBuzz(7)
	expected := "7"

	if result != expected {
		t.Error("Result :", result, " Expected:", expected)
	}
	t.Log("Result :", result, " Expected:", expected)
}

func TestFizzBuzzCase8(t *testing.T) {
	result := FizzBuzz(8)
	expected := "8"

	if result != expected {
		t.Error("Result :", result, " Expected:", expected)
	}
	t.Log("Result :", result, " Expected:", expected)
}

func TestFizzBuzzCase9(t *testing.T) {
	result := FizzBuzz(9)
	expected := "Fizz"

	if result != expected {
		t.Error("Result :", result, " Expected:", expected)
	}
	t.Log("Result :", result, " Expected:", expected)
}

func TestFizzBuzzCase10(t *testing.T) {
	result := FizzBuzz(10)
	expected := "Buzz"

	if result != expected {
		t.Error("Result :", result, " Expected:", expected)
	}
	t.Log("Result :", result, " Expected:", expected)
}

func TestFizzBuzzCase11(t *testing.T) {
	result := FizzBuzz(11)
	expected := "11"

	if result != expected {
		t.Error("Result :", result, " Expected:", expected)
	}
	t.Log("Result :", result, " Expected:", expected)
}

func TestFizzBuzzCase12(t *testing.T) {
	result := FizzBuzz(12)
	expected := "Fizz"

	if result != expected {
		t.Error("Result :", result, " Expected:", expected)
	}
	t.Log("Result :", result, " Expected:", expected)
}

func TestFizzBuzzCase13(t *testing.T) {
	result := FizzBuzz(13)
	expected := "13"

	if result != expected {
		t.Error("Result :", result, " Expected:", expected)
	}
	t.Log("Result :", result, " Expected:", expected)
}

func TestFizzBuzzCase14(t *testing.T) {
	result := FizzBuzz(14)
	expected := "14"

	if result != expected {
		t.Error("Result :", result, " Expected:", expected)
	}
	t.Log("Result :", result, " Expected:", expected)
}

func TestFizzBuzzCase15(t *testing.T) {
	result := FizzBuzz(15)
	expected := "FizzBuzz"

	if result != expected {
		t.Error("Result :", result, " Expected:", expected)
	}
	t.Log("Result :", result, " Expected:", expected)
}
